---
title: "جلسه ۱۳۷"
subtitle: "تکنولوژی‌های مربوط به DevOps"
description: "تکنولوژی‌های مربوط به DevOps"
date: "1396-12-07"
author: "مریم بهزادی"
categories:
    - "sessions"
summaryImage: "/img/posters/poster137.jpg"
readmore: false
---
[![نشست ۱۳۷ شیرازلاگ - تکنولوژی‌های مربوط به DevOps](/img/posters/poster137.jpg)](/img/posters/poster137.jpg)

