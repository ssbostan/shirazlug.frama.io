---
title: "جلسه ۱۷۵"
subtitle: "خودکارسازی فرآیند با انسیبل"
description: "خودکارسازی فرآیند با انسیبل"
keywords: "شیرازلاگ،لینوکس،انسیبل،خودکارسازی فرایند با نسیبل"
date: "1398-06-20"
author: "مریم بهزادی"
draft: false
categories:
    - "sessions"
summaryImage: "/img/posters/poster175.jpg"
readmore: false
---
[![نشست ۱۷۵ - خودکارسازی فرآیند با انسیبل](/img/posters/poster175.jpg)](/img/posters/poster175.jpg)