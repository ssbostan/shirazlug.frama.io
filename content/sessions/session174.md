---
title: "جلسه ۱۷۴"
subtitle: "راهکارهای آموزش و فرهنگ ‌سازی استفاده از نرم افزار آزاد"
description: "راهکارهای آموزش و فرهنگ ‌سازی استفاده از نرم افزار آزاد"
keywords: "شیرازلاگ،نرم افزار آزاد،آموزش"
date: "1398-06-06"
author: "مریم بهزادی"
draft: false
categories:
    - "sessions"
summaryImage: "/img/posters/poster174.jpg"
readmore: false
---
[![نشست ۱۷۴ - راهکارهای آموزش و فرهنگ ‌سازی استفاده از نرم افزار آزاد](/img/posters/poster174.jpg)](/img/posters/poster174.jpg)