---
title: "جلسه ۱۵۲"
subtitle: "غلط‌یابی و استانداردسازی متن فارسی"
description: "غلط‌یابی و استانداردسازی متن فارسی"
date: "1397-06-26"
author: "گودرز جعفری"
draft: false
categories:
    - "sessions"
summaryImage: "/img/posters/poster152.jpg"
readmore: false
---
[![نشست ۱۵۲ شیرازلاگ - غلط‌یابی و استانداردسازی متن فارسی](/img/posters/poster152.jpg)](/img/posters/poster152.jpg)
