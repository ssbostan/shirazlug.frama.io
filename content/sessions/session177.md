---
title: "جلسه ۱۷۷"
subtitle: "هم‌افزایی شماره صفر"
description: "هم‌افزایی شماره صفر"
keywords: "شیرازلاگ، هم افزایی شماره صفر،Synergy #0،Shirazlug "
date: "1398-07-11"
author: "مریم بهزادی"
draft: false
categories:
    - "sessions"
summaryImage: "/img/posters/poster177.jpg"
readmore: false
---
[![نشست ۱۷۷ - هم‌افزایی شماره صفر](/img/posters/poster177.jpg)](/img/posters/poster177.jpg)