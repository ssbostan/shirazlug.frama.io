---
title: "جلسه ۱۳۶"
subtitle: "میزگرد با موضوع جمع‌آوری اطلاعات در اوبونتو"
description: "میزگرد با موضوع جمع‌آوری اطلاعات در اوبونتو"
date: "1396-11-30"
author: "مریم بهزادی"
categories:
    - "sessions"
summaryImage: "/img/posters/poster136.jpg"
readmore: false
---
[![نشست ۱۳۶ شیرازلاگ - میزگرد با موضوع جمع‌آوری اطلاعات در اوبونتو](/img/posters/poster136.jpg)](/img/posters/poster136.jpg)