---
title: "جلسه ۱۵۰"
subtitle: "زبان توصیفی QML"
description: "زبان توصیفی QML"
date: "1397-05-22"
author: "گودرز جعفری"
draft: false
categories:
    - "sessions"
summaryImage: "/img/posters/poster150.jpg"
readmore: false
---
[![نشست ۱۵۰ شیرازلاگ - زبان توصیفی QML](/img/posters/poster150.jpg)](/img/posters/poster150.jpg)
